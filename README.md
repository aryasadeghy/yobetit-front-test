# Backend Nodejs Test


## API URL Document 
you click the link below and access it through post man
## Installation
For install dependencies run the command below
`
    npm install
`
## Run 

for running in development mode 
`
npm run dev
`

for running in production mode
`
npm start
`
## create .env
Step1 : create .env file from  .env.exmaple 
Step2 : change database uri and jwt token


## Deploy On Heroku 

### Install Heroku Cli

You need to install Herku to deply it through your device for installing bases on your operating sysytem 
visit thee link below 
 https://devcenter.heroku.com/articles/heroku-cli#download-and-install

 ### Crete Heroku App
 Create a heroku app and name it: 

 `
heroku create $APP_NAME --buildpack mars/create-react-app
 `

 Check that it worked by running the following command:
 `
  git remote -v
 `
 You should see something like this:


heroku https://git.heroku.com/unique-project-name.git (fetch)
heroku https://git.heroku.com/unique-project-name.git (push)
origin git@gitlab.com:your-username/Your-Git-Repo.git (fetch)
origin git@gitlab.com:your-username/Your-Git-Repo.git (push)



 ### Push to Heroku
`
git push heroku master
`

`
## .env Heroku
For each env variable, you can push it up using:
`
heroku config:set REACT_APP_API_URL=api_url_here
`

That's it Just Run and Enjoy it 

`
 heroku open 
`