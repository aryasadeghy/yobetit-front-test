import React, {useEffect, useState, useMemo} from 'react';
import Layout from '../components/layout';
import Actions from '../store/actions/countryAction';
import {connect} from 'react-redux';
import CountryList from '../components/countryList';
import CountryFilter from '../components/countryFilter'


function Region({getCountries, data, loading}) {
    const [searchFilter, setSearchFilters] = useState({
        search: '',
    });
    useEffect(() => {
        getCountries();
    }, [getCountries]);

    const filtered = useMemo(() => {

        if (data === undefined) {
            return []
        }

        if (searchFilter.length > 0) {
            return data.filter(({name}) => name.toLowerCase().includes(searchFilter.toLowerCase()))
        }
        return data

    }, [searchFilter, data]);

    return (
        <Layout>
            <CountryFilter setSearchFilters={setSearchFilters} searchFilter={searchFilter}/>
            <CountryList loading={loading} data={filtered}/>
        </Layout>
    );
}

const mapDispatchToProps = dispatch => ({
    getCountries: () => dispatch(Actions.getCountries())
});
const mapStateToProps = state => ({
    data: state.country.data,
    loading: state.country.loading
});
export default connect(mapStateToProps, mapDispatchToProps)(Region);
