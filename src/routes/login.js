import React from 'react';
import Layout from '../components/layout';
import Actions from '../store/actions/authAction';
import {connect} from 'react-redux';
import AuthForm from '../components/authForm';
import {useHistory} from "react-router-dom";

function Login({authenticate, loading}) {
    let history = useHistory();

    async function loginHandler(values) {
        try {
            await authenticate(values);
            history.push("/home");

        } catch (e) {

        }
    }

    return (
        <Layout>
            <AuthForm loading={loading} callBack={loginHandler} type="login"/>
        </Layout>
    );
}

const mapDispatchToProps = dispatch => ({
    authenticate: (values) => dispatch(Actions.authenticate(values))
});
const mapStateToProps = state => ({
    loading: state.auth.loading
});
export default connect(mapStateToProps, mapDispatchToProps)(Login);
