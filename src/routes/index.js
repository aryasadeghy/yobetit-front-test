import React from 'react';
import {
    BrowserRouter,
    Switch,
    Route,
    Redirect,
} from "react-router-dom";
import {Provider} from 'react-redux'
import configureStore from '../store';

//Routes Components
import Game from './game';
import Register from './register';
import Login from './login';
import Region from './region';


function App() {
    return (
        <Provider store={configureStore()}>
            <BrowserRouter>
                <Switch>
                    <Route exact path="/game">
                        <Game/>
                    </Route>
                    <Route exact path="/region">
                        <Region/>
                    </Route>
                    <Route exact path="/register">
                        <Register/>
                    </Route>
                    <Route exact path="/login">
                        <Login/>
                    </Route>
                    <Redirect from='*' to='/region'/>
                </Switch>
            </BrowserRouter>
        </Provider>
    );
}

export default App