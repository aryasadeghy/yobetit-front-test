import React from 'react';
import Layout from '../components/layout';
import Actions from '../store/actions/authAction';
import {connect} from 'react-redux';
import AuthForm from '../components/authForm';
import {useHistory} from "react-router-dom";


function Register({register, loading}) {
    let history = useHistory();

    async function registerHandler(values) {
        try {
            await register(values);
            history.push("/login");

        } catch (e) {

        }
    }

    return (
        <Layout>
            <AuthForm loading={loading} callBack={registerHandler} type="register"/>
        </Layout>
    );
}

const mapDispatchToProps = dispatch => ({
    register: (values) => dispatch(Actions.register(values))
});
const mapStateToProps = state => ({
    loading: state.auth.loading
});
export default connect(mapStateToProps, mapDispatchToProps)(Register);
