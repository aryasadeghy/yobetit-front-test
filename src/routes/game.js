import React, {useState} from 'react';
import Layout from '../components/layout';
import SlotMachine from '../components/slotMachine'

function Game() {
    const reel1 = ["cherry", "lemon", "apple", "lemon", "banana", "banana", "lemon", "lemon"];
    const reel2 = ["lemon", "apple", "lemon", "lemon", "cherry", "apple", "banana", "lemon"];
    const reel3 = ["lemon", "apple", "lemon", "apple", "cherry", "lemon", "banana", "lemon"];

    const [selectedReel, setSelectedReel] = useState(false);

    const [coin, setCoin] = useState(20);

    function countCoinGame(value) {
        switch (true) {
            case value.apple === 2:
                return coin + 10;
            case value.apple === 3:
                return coin + 20;
            case value.lemon === 3 :
                return coin + 3;
            case value.banana === 3:
                return coin + 15;
            case value.banana === 2:
                return coin + 5;
            case value.cherry === 3:
                return coin + 40;
            case value.cherry === 2:
                return coin + 50;
            default:
                return coin - 1
        }
    }

    function starGame() {
        if (coin !== 0) {
            const reel1Selected = reel1[Math.floor(Math.random() * (reel1.length - 2)) + 1];
            const reel2Selected = reel2[Math.floor(Math.random() * (reel1.length - 2)) + 1];
            const reel3Selected = reel3[Math.floor(Math.random() * (reel1.length - 2)) + 1];
            const selectedReels = [reel1Selected, reel2Selected, reel3Selected]
            setSelectedReel(selectedReels);
            const counts = selectedReels.reduce((acc, value) => ({
                ...acc,
                [value]: (acc[value] || 0) + 1
            }), {});
            const countGame = countCoinGame(counts);
            setCoin(countGame)
        } else {
            setCoin(20)
        }
    }

    return (
        <Layout>
            <SlotMachine
                coin={coin}
                starGame={starGame}
                reel1={reel1}
                reel2={reel2}
                reel3={reel3}
                selectedReel={selectedReel}
                setSelectedReel={setSelectedReel}
            />
        </Layout>
    );
}


export default Game;
