import styled from 'styled-components'

const Style = styled.div`
    box-shadow: 0 3px 9px rgba(0,0,0,.25);
    background: rgb(238,238,238);
    background: rgb(219,167,28);
    background: linear-gradient(0deg, rgba(219,167,28,1) 15%, rgba(219,169,28,1) 55%, rgba(255,149,46,1) 100%);
    width: 540px;
    margin: 10px auto 20px;
    padding: 20px;
    border-radius: 50px;
    position: relative;
    background: rgb(253,215,29);
    background: linear-gradient(90deg, rgba(253,215,29,1) 24%, rgba(252,176,69,1) 84%);
`;
export const Wrapper = styled.div`
    margin-top: 50px;
    display: flex;
    align-items: center;
    justify-content: center;
`;
export const Start = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const ReelWrapper = styled.div`
    display: flex;
    border-radius: 30px;
    padding: 20px 0 20px 20px;
    width: 80%;
    margin: 15px;

`;
export const Reel = styled.div`

    padding: 0 8px;
    width: 105px;
    height: 100px;
    overflow: hidden;
    margin-right: 20px;
    border-radius: 10px;
    background: #fff;
    box-shadow: 0 2px 7px rgba(0,0,0,.3) inset, 0 0px 1px rgba(0,0,0,.2) inset;
    
    div {
      height: 47px;
      margin-top: 37px;
      text-align: center;
      
      p {
          color: #4CAF50;
          font-size: 1.3rem;
          font-weight: bold;
      }
    }
    
`;
export const Message = styled.div`
    text-align: center;
    color: #000;
    font-size: 1.8em;
    padding: 25px 0 5px;
    text-shadow: 0 -1px 0 rgb(80, 81, 86), 0 1px 0 rgba(255,255,255,.5);
`;
export const CoinWrapper = styled.div`
    position:absolute;
    background: #fff;
    border-radius: 50%;
    box-shadow: 0 2px 7px rgba(0,0,0,.3) inset, 0 0px 1px rgba(0,0,0,.2) inset;
    padding:15px;

`;


export default Style