import React, {useState} from 'react'
import Style, {ReelWrapper, Reel, Start, Wrapper, Message, CoinWrapper,} from './style'
import {Button} from 'antd'

function SlotMachine({reel1, reel2, reel3, starGame, coin, selectedReel, setSelectedReel}) {
    const [spinStyle, setSpinStyle] = useState({
            transform: `translateY(0)`,
        })
    ;

    function spinOn() {
        let spin = 0;
        if (Array.isArray(selectedReel)) {
            setSelectedReel([])
        }
        const timer = setInterval(() => {
            setSpinStyle({
                transform: `translateY(${spin}px)`,
            });
            spin = spin - 10
        }, 10);
        setTimeout(() => {
            starGame()
            clearInterval(timer)
            setSpinStyle({
                transform: `translateY(0px)`,
            });
        }, 600)
    }

    return (
        <Style>
            <CoinWrapper>
                <span>{coin}</span>
            </CoinWrapper>
            <Wrapper>
                <ReelWrapper>
                    <Reel>
                        {selectedReel.length === 0 || selectedReel === false ? reel1.map((item, index) => {
                                return (
                                    <div key={index} style={spinStyle}>
                                        <p>{item}</p>
                                    </div>
                                )
                            }) :
                            <div style={spinStyle}>
                                <p> {selectedReel[0]}</p>
                            </div>
                        }
                    </Reel>
                    <Reel>
                        {selectedReel.length === 0 || selectedReel === false ? reel2.map((item, index) => {
                                return (
                                    <div key={index} style={spinStyle}>
                                        <p key={index}>{item}</p>
                                    </div>
                                )
                            }) :
                            <div style={spinStyle}>
                                <p> {selectedReel[1]}</p>
                            </div>
                        }

                    </Reel>
                    <Reel>
                        {selectedReel.length === 0 || selectedReel === false ? reel3.map((item, index) => {
                                return (
                                    <div key={index} style={spinStyle}>
                                        <p key={index}>{item}</p>
                                    </div>
                                )
                            }) :
                            <div style={spinStyle}>
                                <p> {selectedReel[2]}</p>
                            </div>
                        }
                    </Reel>
                </ReelWrapper>
                <Start>
                    <Button size='large' onClick={spinOn} type='success'>{coin === 0 ? "Restart" : "Start"}</Button>
                </Start>
            </Wrapper>
            <Message>Press Start</Message>
        </Style>
    )
}


export default SlotMachine;