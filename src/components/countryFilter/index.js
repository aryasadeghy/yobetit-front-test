import React from "react";
import {Form, Input} from 'antd';
import Style from './style';

const {Search} = Input;

function FilterCountry({setSearchFilters}) {
    return (
        <Style>
            <Form>
                <Search
                    placeholder="input search text"
                    enterButton="Search"
                    size="large"
                    onSearch={value => setSearchFilters(value)}
                />
            </Form>
        </Style>
    )
}

export default FilterCountry;