import React, {Fragment} from 'react';
import {Layout, Menu} from 'antd';
import Style, {ContentWrapper} from './style';
import {NavLink} from 'react-router-dom';
import {useHistory} from 'react-router-dom';
import Actions from '../../store/actions/authAction'
import {connect} from 'react-redux'

const {Header, Footer, Content} = Layout;

function DefaultLayout(props) {
    const token = localStorage.getItem('token')
    let history = useHistory();

    async function handleLogOut() {
        try {
            await props.deathenticate()
            history.push("/country");
        } catch (e) {

        }
    }

    return (
        <Style>
            <Layout>
                <Header style={{position: 'fixed', zIndex: 1, width: '100%'}}>
                    <div className="logo"/>
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        // defaultSelectedKeys={activeNum}
                        style={{lineHeight: '64px'}}
                    >
                        {
                            token ?
                                <Menu.Item key="1">
                                    <NavLink to="/login" onClick={handleLogOut}>
                                        Log Out
                                    </NavLink>
                                </Menu.Item>
                                :
                                <Fragment></Fragment>
                        }
                        {
                            !token ?
                                <Menu.Item key="1">
                                    <NavLink to="/login">
                                        Login
                                    </NavLink>
                                </Menu.Item>
                                :
                                <Fragment></Fragment>
                        }
                        {
                            !token ?
                                <Menu.Item key="2">
                                    <NavLink to="/register">
                                        Register
                                    </NavLink>
                                </Menu.Item>
                                :
                                <Fragment></Fragment>
                        }
                        <Menu.Item key="3">
                            <NavLink to="/region">
                                Countries
                            </NavLink>
                        </Menu.Item>
                        <Menu.Item key="4">
                            <NavLink to="/game">
                                Slot Machine
                            </NavLink>
                        </Menu.Item>
                    </Menu>
                </Header>
                <Content className='content'>
                    <ContentWrapper>
                        {props.children}
                    </ContentWrapper>
                </Content>
                <Footer className='footer'>Ant Design ©2018 Created by Ant UED</Footer>
            </Layout>
        </Style>
    );
}

const mapDispatchToProps = dispatch => ({
    deathenticate: () => dispatch(Actions.deathenticate())
})

export default connect(state => state, mapDispatchToProps)(DefaultLayout);
