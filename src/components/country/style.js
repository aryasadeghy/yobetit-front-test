import styled from 'styled-components'

const Style = styled.div `
    background: #fff;
    margin : 5px;
    border : 1px solid #f2f2f2;
    padding: 10px;
`;

export const Image = styled.div`
    position:relative;
    height:200px;
    img {
        width:100%;
        height:100%;
        position:absolute;
    }
`

export const Detail = styled.div`
    padding: 20px;
    display:flex;
    flex-direction:column;
    span.name {
        font-weight :bold;
        font-size : 1rem;
    }
    span.code {
        font-weight :bold;
        font-size : 0.6rem;
    }

`;

export default Style