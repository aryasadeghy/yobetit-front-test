import React from 'react'
import Style, {Image, Detail} from './style'
import PropTypes from 'prop-types';
import {LazyLoadImage} from 'react-lazy-load-image-component';


function Country({data}) {
    const {alpha3Code, name, flag} = data;
    return (
        <Style>
            <Image>
                <LazyLoadImage
                    alt={name}
                    src={flag}
                />
            </Image>
            <Detail>
                <span className="name">name : {name} </span>
                <span className="name">
                    country Code  :{alpha3Code}
                </span>
            </Detail>
        </Style>
    )
}

Country.propTypes = {
    data: PropTypes.object.isRequired,
};

export default Country;