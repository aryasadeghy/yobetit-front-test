import React from 'react';
import Style from './style';
import PropTypes from 'prop-types';
import {Form, Icon, Input, Button} from 'antd';
import {Link} from 'react-router-dom';


function AuthForm({callBack, type, form, loading}) {
    const {getFieldDecorator} = form

    function handleSubmit(e) {
        e.preventDefault();
        form.validateFields((err, values) => {
            if (!err) {
                callBack(values)
            }
        });
    };
    return (
        <Style>
            {type === 'login' ?
                <Form onSubmit={handleSubmit} className="login-form">
                    <Form.Item>
                        {getFieldDecorator('email', {
                            rules: [{required: true, message: 'Please input your email!'}],
                        })(
                            <Input
                                prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                placeholder="Username"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [{required: true, message: 'Please input your Password!'}],
                        })(
                            <Input
                                prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                type="password"
                                placeholder="Password"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            Log in
                        </Button>
                        <Link to="/register">register now!</Link>
                    </Form.Item>
                </Form> :
                <Form onSubmit={handleSubmit} className="login-form">
                    <Form.Item>
                        {getFieldDecorator('name', {
                            rules: [{required: true, message: 'Please input your Name!'}],
                        })(
                            <Input
                                placeholder="Name"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('email', {
                            rules: [{required: true, message: 'Please input your Email!'}],
                        })(
                            <Input
                                placeholder="Email"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('password', {
                            rules: [{required: true, message: 'Please input your Password!'}],
                        })(
                            <Input
                                type="password"
                                placeholder="Password"
                            />,
                        )}
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            Register
                        </Button>
                        <Link to="/login">Login</Link>
                    </Form.Item>
                </Form>
            }
        </Style>
    )
}

AuthForm.propTypes = {
    callBack: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired,
};

export default Form.create({})(AuthForm);