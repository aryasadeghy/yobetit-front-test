import React from 'react'
import Style from './style'
import Country from '../country'
import PropTypes from 'prop-types';
import {Spin} from 'antd'

function CountryList({data, loading}) {
    return (
        <Style>
            {loading ?
                <Spin spinning={loading} size='large'/>
                :
                data.length > 0 ?
                    data.map(item => <Country key={item.alpha3Code} data={item}/>)
                    :
                    <div>There Is Nothing To Show</div>
            }
        </Style>
    )
}

CountryList.propTypes = {
    data: PropTypes.array.isRequired,
};

export default CountryList;