//Auth 

export const AUTH_LOADING  = 'AUTH_LOADING';
export const AUTHENTICATE  = 'AUTHENTICATE';
export const DEAUTHENTICATE = 'DEAUTHENTICATE';
export const SET_USER_DATA  = 'SET_USER_DATA';

//Countries 

export const COUNTRY_LOADING = 'COUNTRY_LOADING';
export const COUNTRY_SET = 'COUNTRY_SET';
