import {AUTHENTICATE, DEAUTHENTICATE, AUTH_LOADING, SET_USER_DATA} from '../types';

const initialState = {
    token: null,
    loading: false,
    user: {}
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_DATA:
            return {...state, user: action.payload};
        case AUTH_LOADING:
            return {...state, loading: action.payload};
        case AUTHENTICATE:
            return {token: action.payload};
        case DEAUTHENTICATE:
            return {token: null};
        default:
            return state;
    }
};
