import {COUNTRY_LOADING, COUNTRY_SET} from '../types';

const initialState = {
    data: [],
    loading: false,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case COUNTRY_LOADING:
            return {...state, loading: action.payload};
        case COUNTRY_SET:
            return {...state, data: action.payload};
        default:
            return state;
    }
};
