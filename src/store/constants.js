//Countries End Point

export const COUNTRY_GET_API = 'region/country/getAll';

//Auth End Points

export const LOGIN_API = 'users/login';
export const REGISTER_API = 'users/register';
export const LOGOUT_API = 'users/logout';