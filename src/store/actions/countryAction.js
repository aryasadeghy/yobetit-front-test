import {api} from '../../utils/api';
import {COUNTRY_LOADING,COUNTRY_SET} from '../types'
import * as constants from '../constants';


// gets token from the api and stores it in the redux store and in local Storage
const getCountries = (type) => {
  return async (dispatch) => {
        dispatch({type: COUNTRY_LOADING, payload: true});
        const response = await api.get(constants.COUNTRY_GET_API, type)
        const data = response.data.data
        dispatch({type: COUNTRY_SET, payload: data});
        dispatch({type: COUNTRY_LOADING, payload: false});   
  };
};










export default {
  getCountries,
};
