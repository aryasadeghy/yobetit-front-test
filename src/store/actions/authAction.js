import {api} from '../../utils/api';
import {AUTH_LOADING, AUTHENTICATE, DEAUTHENTICATE, SET_USER_DATA} from '../types'
import * as constants from '../constants';
import {notification} from 'antd'

// gets token from the api and stores it in the redux store and in local Storage
const authenticate = (type) => {
    return async (dispatch) => {
        await dispatch({type: AUTH_LOADING, payload: true});
        const response = await api.post(constants.LOGIN_API, type)
        const token = response.data.token;
        const user = response.data.user
        notification['success']({
            message: 'SuccessFully Loged In'
        });
        localStorage.setItem('token', token)
        dispatch({type: AUTHENTICATE, payload: token});
        dispatch({type: SET_USER_DATA, payload: user});
        await dispatch({type: AUTH_LOADING, payload: false});
    }
};


const register = (type) => {
    return async (dispatch) => {
        dispatch({type: AUTH_LOADING, payload: true});
        await api.post(constants.REGISTER_API, type)
        notification['success']({
            message: 'Success Please Login'
        });
        dispatch({type: AUTH_LOADING, payload: false});
    };
};


const deathenticate = () => {
    return async (dispatch, getState) => {
        const token = localStorage.getItem('token')
        console.log(token)
        await api.setHeader('Authorization', `Bearer ${token}`);
        await api.get(constants.LOGOUT_API, {},);
        localStorage.removeItem('token')
        dispatch({type: DEAUTHENTICATE});
    };
};


export default {
    authenticate,
    register,
    deathenticate
};
