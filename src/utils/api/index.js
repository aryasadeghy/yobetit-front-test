import {create} from 'apisauce';
import {notification} from 'antd';


export const api = create({
    baseURL: `${process.env.REACT_APP_API_URL}/api/`,
});

api.addAsyncRequestTransform(request => async () => {
    // const token = localStorage.get('token');
    // console.log(token)
    // if (token && token !== null && token !== undefined) {
    //   request.headers['Authorization'] = `Bearer ${token}`;
    // }
});
api.addResponseTransform(response => {
    switch (response.status) {
        case 400 :
            notification['error']({
                message: 'ops',
                description: response.data.error
            });
            break;
        case 401:
            break;
        case 403:
            break;
        case 404:
            notification['error']({
                message: 'ops!!',
                description: response.data.error
            });
            break;
        case 500:
            notification['error']({
                message: 'OPS!!',
                description:
                    'Server Error Try Later',
            });
            //server error
            break;
        default:
        // code block
    }
    if (response.status === 200 || response.status === 201) {
        return response;
    } else {
        throw response;
    }
});
